﻿using System;
using System.IO;
using System.Collections.Generic;

class Student
{
    public string lastName;
    public string firstName;
    public string middleName;
    public DateTime birthDate;

    public Student(string lastName, string firstName, string middleName, DateTime birthDate)
    {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDate = birthDate;
    }
}

class Program
{
    static void Main(string[] args)
    {
        List<Student> students = new List<Student>();
        string[] lines = File.ReadAllLines("students.txt");
        foreach (string line in lines)
        {
            string[] parts = line.Split(',');
            string lastName = parts[0].Trim();
            string firstName = parts[1].Trim();
            string middleName = parts[2].Trim();
            DateTime birthDate = DateTime.ParseExact(parts[3].Trim(), "yyyy-MM-dd", null);
            Student student = new Student(lastName, firstName, middleName, birthDate);
            students.Add(student);
        }

        students.Sort((x, y) => x.birthDate.CompareTo(y.birthDate));

        Console.WriteLine("|{0,-15}|{1,-15}|{2,-15}|{3,-15}|", "Last Name", "First Name", "Middle Name", "Birth Date");
        Console.WriteLine("|{0,-15}|{1,-15}|{2,-15}|{3,-15}|", "-----------", "----------", "------------", "----------");
        foreach (Student student in students)
        {
            Console.WriteLine("|{0,-15}|{1,-15}|{2,-15}|{3,-15:yyyy-MM-dd}|", student.lastName, student.firstName, student.middleName, student.birthDate);
        }
    }
}
